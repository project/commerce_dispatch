This module provides a list of rules which can be used for a shipping strategy
and other things.

Unlike commerce_shipping it's not creating an entity on checkout.
It's providing possibilities realize shipping with a "shipping product".
So it is possible to change the price depending on physical products in the
cart and the address of the customer. I've started this module because
I failed in realizing this with the commerce_shipping module. But there
are other possibilities to use this module. Maybe you like to put
automatically a product in the cart with rules for other tasks.
So this module is following the main strategy of commerce to be a framework.
