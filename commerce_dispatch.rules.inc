<?php

/**
 * @file
 * Rules integration for commerce dispatch.
 *
 * @addtogroup rules
 * @{
 */


/**
 * Implements hook_rules_action_info().
 */
function commerce_dispatch_rules_action_info() {
  $actions = array();

  $actions['commerce_dispatch_add_product'] = array(
    'label' => t('Add a product by ID.'),
    'parameter' => array(
    'commerce_order' => array(
      'type' => 'commerce_order',
      'label' => t('Order'),
      'description' => t('The current order.'),
    ),
    'product_id' => array(
      'type' => 'text',
      'label' => t('Product ID'),
      'default value' => 0,
      'description' => t('The ID of the product to be added.'),
    ),
  ),
    'group' => t('Commerce Dispatch'),
    'callbacks' => array(
      'execute' => 'commerce_dispatch_add_product',
    ),
  );

  $actions['commerce_dispatch_add_product_sku'] = array(
    'label' => t('Add a product by SKU.'),
    'parameter' => array(
    'commerce_order' => array(
      'type' => 'commerce_order',
      'label' => t('Order'),
      'description' => t('The current order.'),
    ),
    'product_sku' => array(
      'type' => 'text',
      'label' => t('Product SKU'),
      'default value' => '',
      'description' => t('The SKU of the product to be added.'),
    ),
  ),
    'group' => t('Commerce Dispatch'),
    'callbacks' => array(
      'execute' => 'commerce_dispatch_add_product_sku',
    ),
  );

  return $actions;
}


/**
 * Implements hook_rules_condition_info().
 */
function commerce_dispatch_rules_condition_info() {
  $conditions = array();
  // Clone of commerce_order_compare_total_product_quantity (commerce_order.module)
  $conditions['commerce_dispatch_compare_total_product_quantity'] = array(
    'label' => t('product quantity comparison by product type (warning: extra database call).'),
    'parameter' => array(
      'commerce_order' => array(
        'type' => 'commerce_order',
        'label' => t('Order'),
        'description' => t('The order whose product line item quantities should be totalled. If the specified order does not exist, the comparison will act as if it is against a quantity of 0.'),
      ),
      'operator' => array(
        'type' => 'text',
        'label' => t('Operator'),
        'description' => t('The comparison operator to use.'),
        'default value' => '>=',
        'options list' => 'commerce_numeric_comparison_operator_options_list',
        'restriction' => 'input',
      ),
      'value' => array(
        'type' => 'text',
        'label' => t('Quantity'),
        'default value' => 1,
        'description' => t('The value to compare.'),
      ),
      'producttype' => array(
        'type' => 'text',
        'label' => t('Product Type'),
        'default value' => 'product',
        'description' => t('The product type (machine readable name) to count.'),
      ),

    ),
    'group' => t('Commerce Dispatch'),
    'callbacks' => array(
      'execute' => 'commerce_dispatch_rules_compare_total_quantity',
    ),
  );

    // This is alos a clone of commerce_order_compare_total_product_quantity (commerce_order.module)
    $conditions['commerce_dispatch_compare_total_product_sum'] = array(
    'label' => t('product numeric field total sum comparison'),
    'parameter' => array(
      'commerce_order' => array(
        'type' => 'commerce_order',
        'label' => t('Order'),
        'description' => t('The order whose product line item fields should be summated. If the specified order does not exist, the comparison will act as if it is against a sum of 0.'),
      ),
      'operator' => array(
        'type' => 'text',
        'label' => t('Operator'),
        'description' => t('The comparison operator to use against the total sum of specified fields of all products on the order.'),
        'default value' => '>=',
        'options list' => 'commerce_numeric_comparison_operator_options_list',
        'restriction' => 'input',
      ),
      'value' => array(
        'type' => 'text',
        'label' => t('Value to compare'),
        'default value' => 100,
        'description' => t('The value to compare against the total sum on the order.'),
      ),
      'fieldname' => array(
        'type' => 'text',
        'label' => t('Field to summate'),
        'default value' => '',
        'description' => t('The field name (machine readable name including "field_") to summate. For example you can use "field_weight". You can use e.g. commerce_physical to summarize "weight" but currently you should handle all products in the same weight unit to compare (e.g. only kilogramms). And the field should only have a single value.'),
      ),
      'fieldkey' => array(
        'type' => 'text',
        'label' => t('array key of value'),
        'default value' => '',
        'description' => t('The array key where the value could be found. The physical.module e.g. has the key "weight".'),
      ),
    ),
    'group' => t('Commerce Dispatch'),
    'callbacks' => array(
      'execute' => 'commerce_dispatch_rules_compare_sum',
    ),
  );

  $conditions['commerce_dispatch_line_item_is_product_type'] = array(
    'label' => t('line item is product type (warning: extra database call)'),
    'parameter' => array(
      'line_item' => array(
        'type' => 'commerce_line_item',
        'label' => t('Commere Line Item'),
      ),
    'producttype' => array(
      'type' => 'text',
      'label' => t('Product Type'),
      'default value' => 'product',
      'description' => t('Custom: The product type (machine readable name) to check.'),
      ),
    ),
    'group' => t('Commerce Dispatch'),
      'callbacks' => array(
      'execute' => 'commerce_dispatch_rules_line_item_is_product_type',
    ),
  );

  $conditions['commerce_dispatch_line_item_has_sku'] = array(
    'label' => t('line item has specified SKU.'),
    'parameter' => array(
      'line_item' => array(
        'type' => 'commerce_line_item',
        'label' => t('Commere Line Item'),
      ),
    'product_sku' => array(
      'type' => 'text',
      'label' => t('Product SKU'),
      'default value' => '',
      'description' => t('The SKU of the product to check.'),
      ),
    ),
    'group' => t('Commerce Dispatch'),
      'callbacks' => array(
      'execute' => 'commerce_dispatch_rules_line_item_has_sku',
    ),
  );
  return $conditions;
}


/**
 * @}
 */
