<?php
/**
 * @file
 * commerce_dispatch.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function commerce_dispatch_default_rules_configuration() {
  $items = array();
  $items['rules_commerce_dispatch_add_if_weight'] = entity_import('rules_config', '{ "rules_commerce_dispatch_add_if_weight" : {
      "LABEL" : "commerce_dispatch: add dispatch product by sku if cart has weight",
      "PLUGIN" : "reaction rule",
      "WEIGHT" : "-1",
      "ACTIVE" : false,
      "TAGS" : [ "dispatch" ],
      "REQUIRES" : [ "commerce_dispatch", "commerce_order", "commerce_cart" ],
      "ON" : [ "commerce_cart_product_add" ],
      "IF" : [
        { "commerce_dispatch_compare_total_product_sum" : {
            "commerce_order" : [ "commerce_order" ],
            "operator" : "\u003E",
            "value" : "0",
            "fieldname" : "field_weight",
            "fieldkey" : "weight"
          }
        },
        { "commerce_order_contains_product" : {
            "commerce_order" : [ "commerce_order" ],
            "product_id" : "dispatch",
            "operator" : "\u003C",
            "value" : "1"
          }
        }
      ],
      "DO" : [
        { "commerce_dispatch_add_product_sku" : { "commerce_order" : [ "commerce_order" ], "product_sku" : "dispatch" } }
      ]
    }
  }');
  $items['rules_commerce_dispatch_add'] = entity_import('rules_config', '{ "rules_commerce_dispatch_add" : {
      "LABEL" : "commerce_dispatch: add dispatch product by sku",
      "PLUGIN" : "reaction rule",
      "WEIGHT" : "-1",
      "ACTIVE" : false,
      "TAGS" : [ "dispatch" ],
      "REQUIRES" : [ "commerce_dispatch", "commerce_order", "commerce_cart" ],
      "ON" : [ "commerce_cart_product_add" ],
      "IF" : [
        { "commerce_order_contains_product" : {
            "commerce_order" : [ "commerce_order" ],
            "product_id" : "dispatch",
            "operator" : "\u003C",
            "value" : "1"
          }
        }
      ],
      "DO" : [
        { "commerce_dispatch_add_product_sku" : { "commerce_order" : [ "commerce_order" ], "product_sku" : "dispatch" } }
      ]
    }
  }');
  $items['rules_commerce_dispatch_add_tax'] = entity_import('rules_config', '{ "rules_commerce_dispatch_add_tax" : {
      "LABEL" : "commerce_dispatch: add tax to price",
      "PLUGIN" : "reaction rule",
      "WEIGHT" : "2",
      "ACTIVE" : false,
      "TAGS" : [ "dispatch" ],
      "REQUIRES" : [ "rules", "commerce_product_reference" ],
      "ON" : [ "commerce_product_calculate_sell_price" ],
      "IF" : [
        { "component_rules_commerce_dispatch_set" : { "commerce_line_item" : [ "commerce_line_item" ] } }
      ],
      "DO" : []
    }
  }');
  $items['rules_commerce_dispatch_price_set'] = entity_import('rules_config', '{ "rules_commerce_dispatch_price_set" : {
      "LABEL" : "commerce_dispatch: price set",
      "PLUGIN" : "reaction rule",
      "WEIGHT" : "1",
      "ACTIVE" : false,
      "TAGS" : [ "dispatch" ],
      "REQUIRES" : [ "rules", "commerce_line_item", "commerce_product_reference" ],
      "ON" : [ "commerce_product_calculate_sell_price" ],
      "IF" : [
        { "commerce_dispatch_compare_total_product_quantity" : [] },
        { "component_rules_commerce_dispatch_set" : { "commerce_line_item" : [ "commerce_line_item" ] } }
      ],
      "DO" : [
        { "commerce_line_item_unit_price_amount" : {
            "commerce_line_item" : [ "commerce_line_item" ],
            "amount" : "840",
            "component_name" : "base_price"
          }
        }
      ]
    }
  }');
  $items['rules_commerce_dispatch_reset_price'] = entity_import('rules_config', '{ "rules_commerce_dispatch_reset_price" : {
    "LABEL" : "commerce_dispatch: price reset",
    "PLUGIN" : "reaction rule",
    "ACTIVE" : false,
    "TAGS" : [ "dispatch" ],
    "REQUIRES" : [ "rules", "commerce_dispatch", "commerce_product_reference" ],
    "ON" : [ "commerce_product_calculate_sell_price" ],
    "IF" : [
      { "component_rules_commerce_dispatch_status" : { "commerce_order" : [ "commerce-line-item:order" ] } },
      { "commerce_dispatch_line_item_has_sku" : { "line_item" : [ "commerce-line-item" ], "product_sku" : "dispatch" } }
    ],
    "DO" : [
      { "data_set" : {
          "data" : [ "commerce-line-item:commerce-unit-price:amount" ],
          "value" : "0"
        }
      }
    ]
  }
}');
  $items['rules_commerce_dispatch_set'] = entity_import('rules_config', '{ "rules_commerce_dispatch_set" : {
      "LABEL" : "commerce_dispatch: set",
      "PLUGIN" : "and",
      "TAGS" : [ "dispatch" ],
      "REQUIRES" : [ "rules", "commerce_dispatch" ],
      "USES VARIABLES" : { "commerce_line_item" : { "label" : "Commerce Line item", "type" : "commerce_line_item" } },
      "AND" : [
        { "data_is" : {
            "data" : [ "commerce-line-item:order:status" ],
            "value" : "checkout_review"
          }
        },
        { "commerce_dispatch_line_item_has_sku" : { "line_item" : [ "commerce-line-item" ], "product_sku" : "dispatch" } }
      ]
    }
  }');
  $items['rules_commerce_dispatch_status'] = entity_import('rules_config', '{ "rules_commerce_dispatch_status" : {
      "LABEL" : "commerce_dispatch: reset",
      "PLUGIN" : "or",
      "TAGS" : [ "dispatch" ],
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : { "commerce_order" : { "label" : "Commerce Order", "type" : "commerce_order" } },
      "OR" : [
        { "data_is" : { "data" : [ "commerce-order:state" ], "value" : "canceled" } },
        { "data_is" : { "data" : [ "commerce-order:state" ], "value" : "cart" } },
        { "data_is" : { "data" : [ "commerce-order:status" ], "value" : "checkout_checkout" } },
        { "data_is" : { "data" : [ "commerce-order:status" ], "value" : "checkout_review" } }
      ]
    }
  }');
  return $items;
}
